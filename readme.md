# Introduction

This repository has been setup as a central location for others to submit walkthroughs for Vulnhub machines. 

It's a way to give back to the community, get yourself noticed even more, and contribute towards others learning within the Cyber Security field.

## Adding New Content

### New Boxes

If a new box does not have it's own file yet, please create the proper file for it. You may use the [new box template](https://gitlab.com/vulnhub/walkthrough/-/blob/master/new-box-template.md)

### New Writeups

If a box's file already exists, make your changes by adding a link back to your write up and submit a merge request.

New links should use the following format:

`- [Title Of WalkThough](https://link.to.walkthough) - [@AuthorName (This is optional, delete if you don't want to link social media)](https://link.to.social.media - (This is optional, delete if you don't want to link social media))`

## Social Platforms

* [Discord](https://discord.gg/yNndh7R) - Join and let us know if you submitted a box or walkthrough for a fancy role
* [Twitter](https://twitter.com/vulnhub)
